def img2text(img,word="@",_resize='auto'):
   '''img2text(img,       #dir of img (like ~/test.png in linux or D:\\image\\test.png in window)
   word,                                #word to out image with it(='@')
   resize)                            #resize image (='auto')
   @param _resize: (width, height)
         'auto':{
               width=> width of img
               height => height of img
         }
         '/2','/3',...,'/x':{
               width: width of img /2 or /3 or ....
               height : height of img /2 or /3 or ....
         }
         '*2','*3',...,'*x':{
               width: width of img *2 or *3 or ....
               height : height of img *2 or *3 or ....
         }
         (int(x),int(y)): resize width to 'x' and resize height to 'y'''''
   try:
      from cv2 import imread,resize
   except ModuleNotFoundError:
      print('please install cv2 library')
   try:
      from colorama import init,Fore as c
   except ModuleNotFoundError:
      print('please install colorama library')
   from os.path import isfile
   if not isfile(img) :
      return None
   img = imread(img)
   shp = img.shape[0:2]
   #---------#
   if type(_resize) == tuple:# and len(resize) == 2:
      shp = _resize
   elif type(_resize) == str:
      if _resize.startswith('/'):
         num = int(_resize[1])
         shp = (shp[0]/num,shp[1]/num)
      elif _resize.startswith('*'):
         num = int(_resize[1])
         shp = (shp[0]*num,shp[1]*num)
   img = resize(img,shp)
   #---------#
   out = ''
   init(True)
   for x in img:
      for y in x:
         a=int(y[0]/127)*127+1
         b=int(y[1]/127)*127+1
         d=int(y[2]/127)*127+1
         l=[a,b,d]
         p=None
         if l==[1,1,1]:
            p=c.BLACK
         elif l==[128,128,128]:
            p=c.LIGHTBLACK_EX
         elif l==[255,255,255]:
            p=c.LIGHTWHITE_EX
         elif l==[1,1,128]:
            p=c.RED
         elif l==[1,1,255]:
            p=c.LIGHTRED_EX
         elif l==[1,128,1]:
            p=c.GREEN
         elif l==[1,128,128] or l==[1,128,255]:
            p=c.YELLOW
         elif l==[1,255,1] or l==[1,255,128]:
            p=c.LIGHTGREEN_EX
         elif l==[1,255,255]:
            p=c.LIGHTYELLOW_EX
         elif l==[128,1,1]:
            p=c.BLUE
         elif l==[128,1,128]:
            p=c.MAGENTA
         elif l==[128,1,255]:
            p=c.LIGHTMAGENTA_EX
         elif l==[128,128,1]:
            p=c.CYAN
         elif l==[128,128,128]:
            p=c.LIGHTBLACK_EX
         elif l==[128,128,255]:
            p=c.LIGHTMAGENTA_EX
         elif l==[128,255,1] or l==[128,255,128]:
            p=c.LIGHTGREEN_EX
         elif l==[128,255,255]:
            p=c.LIGHTYELLOW_EX
         elif l==[255,1,1]:
            p=c.LIGHTBLUE_EX
         elif l==[255,1,128]:
            p=c.MAGENTA
         elif l==[255,1,255]:
            p=c.LIGHTMAGENTA_EX
         elif l==[255,128,1]:
            p=c.CYAN
         elif l==[255,128,128]:
            p=c.BLUE
         elif l==[255,128,255]:
            p=c.LIGHTMAGENTA_EX
         elif l==[255,255,1] or l==[255,255,128]:
            p=c.LIGHTCYAN_EX
         out += p+word
      out += '\n'
   return out

#creator : PyCdr
#in gitlab : https://gitlab.com/pycdr
#in tel : @pycdr_pycdr
