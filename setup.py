try:
    from setuptools import setup
except ModuleNotFoundError:
    tORf = input('module \"setuptools\" not found, install it(sudo apt-get install python3-setuptools)[y->yes/ANYKEY->no]? ').lower()
    if tORf == 'y':
        from os import system
        print("OK, please wait to install it.")
        system("sudo apt-get install python3-setuptools")
        print("installed.")
    else:
        exit()
setup(
  name = 'img2text',
  version = '0.1',
  description= 'img2text Package',
  author = 'PyCdr',
  packages=['image2text']
)
